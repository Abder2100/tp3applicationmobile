package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final Main main = null;
    public final Wind wind = null;
    public final Clouds clouds = null;
    public final Long dt = null;
    public final List<Condition> weather =null;

    public static class Main {
        public final Float temp = null;
        public final Integer humidity = null;
    }
    public static class Wind {
        public final Float speed = null;
        public final Float deg = null;

    }
    public static class Condition {
        public final String description= null;
        public final String icon= null;
    }
    public static class Clouds{
        public final Integer all= null;
    }
}