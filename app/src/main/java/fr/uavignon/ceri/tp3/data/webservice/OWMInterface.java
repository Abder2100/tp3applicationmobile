package fr.uavignon.ceri.tp3.data.webservice;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMInterface {
    @GET("weather")
    Call<WeatherResponse> loadWeatherCity(@Query("q") String name,@Query("appid") String appid);
}
